<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'glissanm_siteone');

/** MySQL database username */
define('DB_USER', 'glissanm_siteone');

/** MySQL database password */
define('DB_PASSWORD', '1([9pfSsI2');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'cwvpdmmfbbbrniri4ec4djkljjb7ie2l4gyfdasu8h1qhzl4zduzzorchq4d5h8i');
define('SECURE_AUTH_KEY',  '92yvwxrfyylv4il0rhfghd801czrtt9l2fe0fjxaxef4eafyv1v5pwtfsswyo0zz');
define('LOGGED_IN_KEY',    'g8uiv70sh78hklew2hyr3tasq0ujgcnnmfugo0dzx3cek6k3lc7wpi6edfrplzyt');
define('NONCE_KEY',        'mt53nf7bdt5g4bhlzfanrlja7ihxok0ixxchwz1pxexfmttdzdvqzysa61q6mtcd');
define('AUTH_SALT',        'tyfwgwy1uxjhcbyx5huskqwn1gy5itk6gbbgnfwq7kote84p5hi0uom13vut8c3r');
define('SECURE_AUTH_SALT', 'bi9oqhbetzfgtyoolmujsbnys9lnkufd3nckziubfr9ylliadpzryhbskpujwvfj');
define('LOGGED_IN_SALT',   'ciocilmlltexcqk9e6n7jtneezrxxadldqaj3whbpbzmkiaihgevzbhyhxhsxlmf');
define('NONCE_SALT',       'e00p1qq2zm8clmmpcz8e4prfyqpjacam2gsorls8l3vmfpzguhhkrsmelx9z25ve');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
